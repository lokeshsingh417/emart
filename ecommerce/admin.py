from django.contrib import admin

from .models import (Cart,
					Discount,
					Admin,
					Customer,
					Category,
					Product,
					DeliveryRecord,
					Addresses,
					PaymentGateway,
					WishList,
					PurchaseRecord,
					FeedBack)

admin.site.site_header = "Ecommerce"

admin.site.register([Cart,Discount,Admin,Customer,Category,Product,DeliveryRecord,Addresses,PaymentGateway,WishList,PurchaseRecord,FeedBack])
# Register your models here.
