from .models import (Cart,
					Discount,
					Admin,
					Customer,
					Category,
					Product,
					DeliveryRecord,
					Addresses,
					PaymentGateway,
					WishList,
					PurchaseRecord,
					FeedBack)
from rest_framework import serializers



class CartSerializer(serializers.ModelSerializer):
	class Meta:
		model = Cart
		fields= "__all__"


class DiscountSerializer(serializers.ModelSerializer):
	class Meta:
		model = Discount
		fields= "__all__"

class CustomerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Customer
		fields= "__all__"

class CategorySerializer(serializers.ModelSerializer):
	class Meta:
		model = Category
		fields= "__all__"

class ProductSerializer(serializers.ModelSerializer):
	class Meta:
		model = Product
		fields= "__all__"

class DeliveryRecordSerializer(serializers.ModelSerializer):
	class Meta:
		model = DeliveryRecord
		fields= "__all__"

class AddressesSerializer(serializers.ModelSerializer):
	class Meta:
		model = Addresses
		fields= "__all__"

class PaymentGatewaySerializer(serializers.ModelSerializer):
	class Meta:
		model = PaymentGateway
		fields= "__all__"

class WishListSerializer(serializers.ModelSerializer):
	class Meta:
		model = WishList
		fields= "__all__"

class PurchaseRecordSerializer(serializers.ModelSerializer):
	class Meta:
		model = PurchaseRecord
		fields= "__all__"

class FeedBackSerializer(serializers.ModelSerializer):
	class Meta:
		model = FeedBack
		fields= "__all__"

class AdminSerializer(serializers.ModelSerializer):
	class Meta:
		model = Admin
		fields= "__all__"


