from django.db import models
import uuid
from django.contrib.auth.models import User as Super_Admin
import random
# Create your models here.

#### REFER CODE
def generateReferCode():
    a = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    refer_code=""
    for i in range(6):
	    refer_code+=random.choice(a)
    return refer_code

###


class Admin(models.Model):
	admin_id=models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	name= models.CharField(max_length=200)
	super_admin_id=models.ForeignKey(Super_Admin,on_delete=models.CASCADE)
	contact_no=models.CharField(max_length=10)
	email_address=models.EmailField(unique=True)
	password=models.CharField(max_length=100)
	avator=models.ImageField(upload_to="images/admin")
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)
	def __str__(self):
		return self.name

class Category(models.Model):
	category_id=models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	name = models.CharField(max_length=200) 
	description=models.TextField()
	image=models.ImageField(upload_to="images/category")
	admin_id=models.ForeignKey(Admin,on_delete=models.CASCADE)
	super_admin_id=models.ForeignKey(Super_Admin,on_delete=models.CASCADE)
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)
	def __str__(self):
		return self.name

class Product(models.Model):
	product_id=models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	name = models.CharField(max_length=200)
	description=models.TextField()
	image=models.ImageField(upload_to="images/product")
	base_price=models.FloatField()
	sell_price=models.FloatField()
	sale_price=models.FloatField()
	total_no=models.IntegerField()
	category_id=models.ForeignKey(Category,on_delete=models.CASCADE)
	admin_id=models.ForeignKey(Admin,on_delete=models.CASCADE)
	super_admin_id=models.ForeignKey(Super_Admin,on_delete=models.CASCADE)
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)
	def __str__(self):
		return self.name

class Customer(models.Model):
	customer_id=models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	name=models.CharField(max_length=200)
	contact_no=models.CharField(max_length=10)
	avator=models.ImageField(upload_to="images/customer")
	email_address=models.EmailField(unique=True)
	refer_code=models.CharField(max_length=6,default=generateReferCode)
	user_name=models.CharField(max_length=100,unique=True)
	password=models.CharField(max_length=100)
	admin_id=models.ForeignKey(Admin,on_delete=models.CASCADE)
	super_admin_id=models.ForeignKey(Super_Admin,on_delete=models.CASCADE)
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)	
	def __str__(self):
		return self.name
class Cart(models.Model):
	cart_id=models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	product_id=models.ForeignKey(Product,on_delete=models.CASCADE)
	customer_id=models.ForeignKey(Customer,on_delete=models.CASCADE)
	expire_date=models.DateTimeField()
	admin_id=models.ForeignKey(Admin,on_delete=models.CASCADE)
	super_admin_id=models.ForeignKey(Super_Admin,on_delete=models.CASCADE)
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)
	def __str__(self):
		return str(self.cart_id)

class WishList(models.Model):
	wishlist_id=models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	admin_id=models.ForeignKey(Admin,on_delete=models.CASCADE)
	super_admin_id=models.ForeignKey(Super_Admin,on_delete=models.CASCADE)
	product_id=models.ForeignKey(Product,on_delete=models.CASCADE)
	customer_id=models.ForeignKey(Customer,on_delete=models.CASCADE)
	expire_date=models.DateTimeField()
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=	models.DateTimeField(auto_now=True)
	def __str__(self):
		return str(self.wishlist_id)
class PaymentGateway(models.Model):
	payment_id=models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	card_no=models.CharField(max_length=20)
	cvv_no=models.IntegerField()
	expire_date=models.DateTimeField()
	admin_id=models.ForeignKey(Admin,on_delete=models.CASCADE)
	super_admin_id=models.ForeignKey(Super_Admin,on_delete=models.CASCADE)
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)
	def __str__(self):
		return str(self.card_no)

class FeedBack(models.Model):
	feedback_id=models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	rating=models.IntegerField()
	image=models.ImageField(upload_to="images/feedback",null=True,blank=True)
	comment=models.TextField()
	product_id=models.ForeignKey(Product,on_delete=models.CASCADE)
	customer_id=models.ForeignKey(Customer,on_delete=models.CASCADE)
	admin_id=models.ForeignKey(Admin,on_delete=models.CASCADE)
	super_admin_id=models.ForeignKey(Super_Admin,on_delete=models.CASCADE)
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)
	def __str__(self):
		return str(self.rating)


class PurchaseRecord(models.Model):
	order_id=models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	transaction_id=models.CharField(max_length=100)
	product_id=models.ForeignKey(Product,on_delete=models.CASCADE)
	transaction_date=models.DateTimeField()
	admin_id=models.ForeignKey(Admin,on_delete=models.CASCADE)
	super_admin_id=models.ForeignKey(Super_Admin,on_delete=models.CASCADE)
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)
	def __str__(self):
		return str(order_id)

class Discount(models.Model):
	discount_variety = (("percent","Percentage"),("Fix Value","Value"))
	discount_id= models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	discount_type=models.CharField(max_length=100,choices=discount_variety)
	name=models.CharField(max_length=100)
	no_of_usuage=models.IntegerField(default=3)
	expire_date=models.DateTimeField()
	admin_id=models.ForeignKey(Admin,on_delete=models.CASCADE)
	super_admin_id=models.ForeignKey(Super_Admin,on_delete=models.CASCADE)
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)
	def __str__(self):
		return self.name


class Addresses(models.Model):
	address_variety= (("Home","Home"),("Work","Work"))
	address_id=models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	address_line_1=models.CharField(max_length=100)
	address_line_2=models.CharField(max_length=100,null=True,blank=True)
	address_type=models.CharField(max_length=100,choices=address_variety)
	pincode=models.CharField(max_length=100)
	state=models.CharField(max_length=100)
	city=models.CharField(max_length=100)
	country=models.CharField(max_length=100)
	landmark=models.CharField(max_length=100)
	street_name=models.CharField(max_length=100,null=True,blank=True)
	admin_id=models.ForeignKey(Admin,on_delete=models.CASCADE)
	super_admin_id=models.ForeignKey(Super_Admin,on_delete=models.CASCADE)
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now_add=True)
	def __str__(self):
		return self.address_line_1 + " "+self.address_line_2+" "+self.pincode

class DeliveryRecord(models.Model):
	track_id=models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	address_id = models.ForeignKey(Addresses,on_delete=models.CASCADE)
	delivery_status=models.CharField(max_length=100)
	customer_id=models.ForeignKey(Customer,on_delete=models.CASCADE)
	product_id=models.ForeignKey(Product,on_delete=models.CASCADE)
	admin_id=models.ForeignKey(Admin,on_delete=models.CASCADE)
	super_admin_id=models.ForeignKey(Super_Admin,on_delete=models.CASCADE)
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)
	def __str__(self):
		return str(self.track_id)

