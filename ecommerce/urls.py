from rest_framework.routers import DefaultRouter
from .views import (CartViewSet,
					DiscountViewSet,
					AdminViewSet,
					CustomerViewSet,
					CategoryViewSet,
					ProductViewSet,
					DeliveryRecordViewSet,
					AddressesViewSet,
					PaymentGatewayViewSet,
					WishListViewSet,
					PurchaseRecordViewSet,
					FeedBackViewSet)


# setting default router 
router = DefaultRouter()

# registering viewset with router
# router.register()
router.register('cart',CartViewSet,basename="cart")

router.register('discount',DiscountViewSet,basename="discount")

router.register('admin',AdminViewSet,basename="admin")

router.register('customer',CustomerViewSet,basename="customer")

router.register('category',CategoryViewSet,basename="category")

router.register('product',ProductViewSet,basename="product")

router.register('deliveryrecord',DeliveryRecordViewSet,basename="deliveryrecord")

router.register('addresses',AddressesViewSet,basename="addresses")

router.register('paymentgateway',PaymentGatewayViewSet,basename="paymentgateway")

router.register('wishlist',WishListViewSet,basename="wishlist")

router.register('purchaserecord',PurchaseRecordViewSet,basename="purchaserecord")

router.register('feedback',FeedBackViewSet,basename="feedback")

# adding it to urlpatterns
urlpatterns = router.urls