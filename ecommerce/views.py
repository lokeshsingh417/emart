from django.shortcuts import render
from rest_framework import viewsets
from .models import (Cart,
					Discount,
					Admin,
					Customer,
					Category,
					Product,
					DeliveryRecord,
					Addresses,
					PaymentGateway,
					WishList,
					PurchaseRecord,
					FeedBack)
from .serializer import (CartSerializer,
					DiscountSerializer,
					AdminSerializer,
					CustomerSerializer,
					CategorySerializer,
					ProductSerializer,
					DeliveryRecordSerializer,
					AddressesSerializer,
					PaymentGatewaySerializer,
					WishListSerializer,
					PurchaseRecordSerializer,
					FeedBackSerializer)


# Create your views here.
class CartViewSet(viewsets.ModelViewSet):
	serializer_class = CartSerializer
	queryset = Cart.objects.all()

class DiscountViewSet(viewsets.ModelViewSet):
	serializer_class = DiscountSerializer
	queryset = Discount.objects.all()

class AdminViewSet(viewsets.ModelViewSet):
	serializer_class = AdminSerializer
	queryset = Admin.objects.all()

class CustomerViewSet(viewsets.ModelViewSet):
	serializer_class = CustomerSerializer
	queryset = Customer.objects.all()

class CategoryViewSet(viewsets.ModelViewSet):
	serializer_class = CategorySerializer
	queryset = Category.objects.all()

class ProductViewSet(viewsets.ModelViewSet):
	serializer_class = ProductSerializer
	queryset = Product.objects.all()

class DeliveryRecordViewSet(viewsets.ModelViewSet):
	serializer_class = DeliveryRecordSerializer
	queryset = DeliveryRecord.objects.all()

class AddressesViewSet(viewsets.ModelViewSet):
	serializer_class = AddressesSerializer
	queryset = Addresses.objects.all()

class PaymentGatewayViewSet(viewsets.ModelViewSet):
	serializer_class = PaymentGatewaySerializer
	queryset = PaymentGateway.objects.all()

class WishListViewSet(viewsets.ModelViewSet):
	serializer_class = WishListSerializer
	queryset = WishList.objects.all()

class PurchaseRecordViewSet(viewsets.ModelViewSet):
	serializer_class = PurchaseRecordSerializer
	queryset = PurchaseRecord.objects.all() # select * from purchaseRecord

class FeedBackViewSet(viewsets.ModelViewSet):
	serializer_class = FeedBackSerializer
	queryset = FeedBack.objects.all()



